//
//  RLAppDelegate.h
//  Reto-Uno
//
//  Created by dcl17 on 04/02/14.
//  Copyright (c) 2014 dcl17. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
