//
//  RLViewController.m
//  Reto-Uno
//
//  Created by dcl17 on 04/02/14.
//  Copyright (c) 2014 dcl17. All rights reserved.
//

#import "RLViewController.h"
#import "CoreLocation/CoreLocation.h"

static NSString *url= @"https://api.foursquare.com/v2/venues/search?&radius=800&locale=es&v=20140121&client_secret=OV3JVHFAU32WALYUZ1PJOHUDU2PXP5E5CUEQC2QJC5JAKWPO&client_id=QZVLKFPQ5MNXF3ISLUNYVBTEENWKEMZ3FZMEOKEAIYW1FWLS";


@interface RLViewController ()<UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate>
@property (weak, nonatomic)IBOutlet UITextField *data;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *myContrasint;
@property (weak,nonatomic) IBOutlet NSLayoutConstraint *myConstraint2;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *venues;
@property (weak, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSIndexPath *lastSelectedIndexPath;
@property (nonatomic, readonly) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *lastLocation;
@property (weak, nonatomic) NSString *query;
@end

@implementation RLViewController
@synthesize locationManager=_locationManager;


#pragma mark - Getter selectors
- (CLLocationManager *) locationManager {
    if (!_locationManager){
        _locationManager= [[CLLocationManager alloc]init];
        _locationManager.desiredAccuracy=kCLLocationAccuracyBest;
        _locationManager.headingFilter=kCLHeadingFilterNone;
        _locationManager.delegate=self;
    }
    return _locationManager;
}


-(void)locationManager: (CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *lastLocation=[locations lastObject];
    NSLog(@"latitud %g, longitud %g",lastLocation.coordinate.latitude, lastLocation.coordinate.longitude);
    if (!self.lastLocation){
        self.lastLocation=lastLocation;
        [self.refreshControl beginRefreshing];
        [self.tableView setContentOffset:CGPointMake(0.0, -90.0) animated:YES];
        [self refreshInfo: self.refreshControl];
    }
    else{
        self.lastLocation=lastLocation;
    }
}
-(void)customizeViewInterface{
    self.title=@"Venues";
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshInfo:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    self.refreshControl= refreshControl;
    self.refreshControl.tintColor=[UIColor grayColor];
    
}


-(void)refreshInfo:(UIRefreshControl *)sender{
    self.query=@"&query=";
    self.query= [self.query stringByAppendingString:self.data.text];
    
    NSString *venueURLString= [NSString stringWithFormat:@"%@&ll=%g,%g%@",url, self.lastLocation.coordinate.latitude, self.lastLocation.coordinate.longitude, self.query];
    NSLog(@"%@", venueURLString);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:venueURLString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!error){
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            self.venues = [json valueForKeyPath:@"response.venues"];
            
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self.tableView reloadData];
                [sender endRefreshing];
            }];
        }
        else{
            NSLog(@"Hubo un error");
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [sender endRefreshing];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Venues" message:@"No fue posible extraer la información. Verifique su conexión a Internet." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
            }];
        }
        
    }];
    [dataTask resume];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.venues.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier= @"cellIdentifier";
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell){
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary *venueInfo= self.venues[indexPath.row];
    
    cell.textLabel.text=venueInfo[@"name"];
    cell.detailTextLabel.textColor=[UIColor grayColor];
    cell.detailTextLabel.text=[venueInfo valueForKeyPath:@"location.address"];
    
    cell.backgroundColor = [UIColor colorWithRed:22/256.0 green:149/256.0 blue:163/256.0 alpha:1.0];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.textLabel.textColor=[UIColor whiteColor];
    
    return cell;
}

- (IBAction)touchButton:(id)sender {
    if(![self.data.text isEqualToString:@""]){
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.myContrasint.constant = -200;
                             self.myConstraint2.constant = -800;
                             self.data.alpha = 0;
                             self.button.alpha = 0;
                             [self.view layoutIfNeeded];
                         }];
        [UIView animateWithDuration:1.7
                         animations:^{
                             self.tableView.alpha = 1;
                             [self.view layoutIfNeeded];
                         }];
        
        [self.tableView deselectRowAtIndexPath:self.lastSelectedIndexPath animated:YES];
        
        if(self.lastLocation){
            [self.refreshControl beginRefreshing];
            [self refreshInfo:self.refreshControl];
        }
        
        if (!self.venues.count) {
            [self.refreshControl beginRefreshing];
            [self refreshInfo:self.refreshControl];
        }
        [self.locationManager startUpdatingLocation];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Ingresa texto en el campo" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.data resignFirstResponder];
    [self.nextResponder touchesEnded:touches withEvent:event];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.tableView.alpha = 0;
    self.view.backgroundColor = [UIColor colorWithRed:22/256.0 green:149/256.0 blue:163/256.0 alpha:1.0];
}


-(void)viewDidLoad{
    [super viewDidLoad];
    [self customizeViewInterface];
    self.tableView.backgroundColor = [UIColor colorWithRed:22/256.0 green:149/256.0 blue:163/256.0 alpha:1.0];
}


@end
