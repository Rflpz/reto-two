//
//  main.m
//  Reto-Uno
//
//  Created by dcl17 on 04/02/14.
//  Copyright (c) 2014 dcl17. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RLAppDelegate class]));
    }
}
